########################################################################################################################
### Imports
########################################################################################################################
import os, zipfile
from datetime import datetime

#-----------------------------------------------------------------------------------------------------------------------
#-- Backup carpeta
#-----------------------------------------------------------------------------------------------------------------------
def backup_to_zip(folder):
    folder_path = os.path.abspath(folder)

    number = 1

    while True:
        zipFileName = os.path.basename(folder) + '_' + str(number) + '.zip'
        if not os.path.exists(zipFileName):
            break
        number += 1

    print('Comprimindo', end = '')
    with zipfile.ZipFile(zipFileName, 'w') as backupZip:
        for folderName, subFolders, fileNames in os.walk(folder):
            backupZip.write(folderName)

            for fileName in fileNames:
                backupZip.write(os.path.join(folderName, fileName))
                print('.', end = '')

    print()
    print('Fin da compresión')
