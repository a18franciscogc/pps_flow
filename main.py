########################################################################################################################
### Imports
########################################################################################################################
import requests
import backup

#-----------------------------------------------------------------------------------------------------------------------
#-- procesar respostas
#-----------------------------------------------------------------------------------------------------------------------
def proc_data(res):
    result = None

    if (res != None):
        content = res.text.replace('\n', '').split('|')

        op = content[0]
        cmd1 = content[1].split(':')
        cmd2 = content[2].split(':')
        auth = content[3].split(':')

        result = (op, cmd1, cmd2, auth)

    return result

#-----------------------------------------------------------------------------------------------------------------------
#-- main
#-----------------------------------------------------------------------------------------------------------------------
def main():
    url = 'https://bitbucket.org/a18franciscogc/pps_flow/raw/main/flow.txt'

    res = requests.get(url)

    op, cmd1, cmd2, auth = proc_data(res)

    print ('operacion   : {}'.format(op))
    print ('comando1    : {}'.format(cmd1))
    print ('comando2    : {}'.format(cmd2))
    print ('autorizado  : {}'.format(auth))

    if cmd1[1] == '1' and auth[1] == '1':
        backup.backup_to_zip('copiar')
    elif cmd1[1] == '0':
        print ('Non hai ningunha operacion que realizar')
    elif auth[1] == '0':
        print ('Operación non autorizada')

########################################################################################################################
### Execución principal
########################################################################################################################
if __name__ == '__main__':
    main()
